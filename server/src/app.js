import express from "express";
import { Appointments } from "./appointments";
import { Customers } from "./customers";
import morgan from "morgan";

export function buildApp(customerData, appointmentData, timeSlots) {
  const app = express();

  const customers = new Customers(customerData);
  const appointments = new Appointments(appointmentData, timeSlots);

  app.use(express.static("dist"));
  app.use(express.json());
  app.use(morgan("dev"));

  app.get("/availableTimeSlots", (req, res) => {
    res.json(appointments.getTimeSlots());
  });

  app.get("/appointments/:from-:to", (req, res) => {
    res.json(
      appointments.getAppointments(
        parseInt(req.params.from),
        parseInt(req.params.to),
        customers.all()
      )
    );
  });

  app.post("/appointments", (req, res) => {
    const appointment = req.body;
    if (appointments.isValid(appointment)) {
      appointments.add(appointment);
      res.sendStatus(201);
    } else {
      const errors = appointments.errors(appointment);
      res.status(422).json({ errors });
    }
  });

  app.post("/customers", (req, res) => {
    const customer = req.body;
    if (customers.isValid(customer)) {
      const customerWithId = customers.add(customer);
      res.status(201).json(customerWithId);
    } else {
      const errors = customers.errors(customer);
      res.status(422).json({ errors });
    }
  });

  app.get("/customers", (req, res) => {
    const results = customers.search(buildSearchParams(req.query));
    res.json(results);
  });

  app.get("*", function (req, res) {
    res.sendFile("dist/index.html", {
      root: process.cwd(),
    });
  });

  return app;
}

function buildSearchParams({
  searchTerm,
  after,
  limit,
  orderBy,
  orderDirection,
}) {
  const searchParams = {};
  if (searchTerm) searchParams.searchTerms = buildSearchTerms(searchTerm);
  if (after) searchParams.after = parseInt(after);
  if (limit) searchParams.limit = parseInt(limit);
  if (orderBy) searchParams.orderBy = orderBy;
  if (orderDirection) searchParams.orderDirection = orderDirection;
  return searchParams;
}

function buildSearchTerms(searchTerm) {
  if (!searchTerm) return undefined;
  if (Array.isArray(searchTerm)) {
    return searchTerm;
  }
  return [searchTerm];
}
